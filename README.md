# 麦当劳拼单反向代理服务器
  实现多个用户在不同电脑登录同一账号进行点单的功能。

# 依赖的开发环境
- NodeJS v8.11.1
- MySQL v5.7.22

# 开发环境运行
- 下载代码
```
git clone https://layverns@bitbucket.org/layverns/mc-proxy.git
```
- 安装依赖
```
cd mc-proxy
npm install
```
- 运行
```
mysql -u root -p
在mysql命令行环境下执行 source ./mc_dev.sql
```
启动mysql服务器，连接mysql数据库导入mc_dev.sql，创建开发时用的数据库和表结构

```
npm run dev
```
启动服务器，运行时监控文件改动，自动执行typescript编译和服务器重启

打开浏览器1访问http://localhost:3000/，进行登录，开始点单
同时打开浏览器2访问http://localhost:3000，登录同一账号，进行点单，两个浏览器的订单会合并在一起

http://localhost:3000/admin/login.html 访问简易后台管理系统

http://localhost:3000/api/v1/signup 也可使用restful api访问，详情参照api文档
- api文档
```
npm run doc
```
生成restful api的文档，文档在apidoc目录下
- 测试
```
npm test
```
执行测试并产生覆盖率报告，覆盖率报告在coverage目录下（proxy的测试中出了一些问题，所以proxy模块的覆盖率不高，问题还在解决中）


# 部署
- 下载代码
```
git clone https://layverns@bitbucket.org/layverns/mc-proxy.git
```
- 安装依赖
```
cd mc-proxy
npm install
```
- 运行
```
mysql -u root -p
在mysql命令行环境下执行 source ./mc.sql
```
启动mysql服务器，连接mysql数据库导入mc.sql，创建开发时用的数据库和表结构

```
npm run build
npm start
```
启动服务器

服务器绑定域名，使用不同的二级域名登录服务器便可实现cookie隔离，例如：a.mc.com登录其中一个账号，b.mc.com登录另一个账号
