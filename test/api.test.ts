import request = require('supertest');
import server from "../src/server";
import 'mocha';
import { expect } from 'chai';

import knex = require('knex');
import config from "../src/config/index";

const db = knex({
    client: 'mysql',
    connection: config.mysql
});

let user = {
    username: 'john2',
    password: '123'
};
let token;

let cookieInit = {username: 'aaaaaa', password: 'aaaaaa', cookie: 'aaaaaa'};
let cookieInitNum = 0;
function generateCookie() {
    return (function () {
        cookieInitNum++;
        return {username: cookieInit.username + cookieInitNum, password: cookieInit.password + cookieInitNum, cookie: cookieInit.cookie + cookieInitNum}
    })();
}


describe("api test", () => {
    before(async function () {
        await db('user').del();
        await db('cookies').del();
    });

    after(async function () {
        await db('user').del();
        await db('cookies').del();
    });

    it("/api/v1/signup 用户注册", () => {
        return request(server).post('/api/v1/signup')
            .send(user)
            .expect(200)
            .then(res => {
                expect(res.body.code).to.equal(0);
            });
    });

    it("/api/v1/login 用户登录获取token", () => {
        return request(server).get('/api/v1/login')
            .query(user)
            .expect(200)
            .then(res => {
                expect(res.body.code).to.equal(0);
                expect(res.body.data.token).to.be.a('string');

                token = res.body.data.token;
            });
    });

    it("/api/v1/cookies 获取cookie列表", async function ()  {
        let cookies = [generateCookie(), generateCookie(), generateCookie(), generateCookie()];
        await db('cookies').insert(cookies);

        return request(server).get('/api/v1/cookies')
            .set('token', token)
            .expect(200)
            .then(res => {
                expect(res.body.code).to.equal(0);
                expect(res.body.data.cookies[0]).to.deep.include(cookies[0]);
                expect(res.body.data.cookies[1]).to.deep.include(cookies[1]);
                expect(res.body.data.cookies[2]).to.deep.include(cookies[2]);
                expect(res.body.data.cookies[3]).to.deep.include(cookies[3]);

            });
    });

    it("/api/v1/cookies/:id 获取一个cookie", async function ()  {
        let cookie = generateCookie();
        let cookieIds = await db('cookies').insert(cookie);

        return request(server).get('/api/v1/cookies/' + cookieIds[0])
            .set('token', token)
            .expect(200)
            .then(res => {
                console.log(res.body);

                expect(res.body.code).to.equal(0);
                expect(res.body.data.cookie.username).to.equal(cookie.username);
                expect(res.body.data.cookie.password).to.equal(cookie.password);
                expect(res.body.data.cookie.cookie).to.equal(cookie.cookie);
            });
    });

    it("/api/v1/cookies 创建一个cookie", async function ()  {
        let cookie = generateCookie();

        return request(server).post('/api/v1/cookies')
            .set('token', token)
            .send(cookie)
            .expect(200)
            .then(res => {
                expect(res.body.code).to.equal(0);
                expect(res.body.data.cookieId).to.not.be.null;
            });
    });

    it("/api/v1/cookies 更新一个cookie", async function ()  {
        let cookie = generateCookie();
        let cookieIds = await db('cookies').insert(cookie);
        let cookieNew = {id: cookieIds[0], username: cookie.username + 'new', password: cookie.password + 'new', cookie: cookie.cookie + 'new'};

        return request(server).put('/api/v1/cookies/')
            .set('token', token)
            .send(cookieNew)
            .expect(200)
            .then(res => {
                expect(res.body.code).to.equal(0);
            });
    });

    it("/api/v1/cookies/:id 删除一个cookie", async function ()  {
        let cookie =  generateCookie();
        let cookieIds = await db('cookies').insert(cookie);

        return request(server).delete('/api/v1/cookies/' + cookieIds[0])
            .set('token', token)
            .expect(200)
            .then(res => {
                expect(res.body.code).to.equal(0);
            });
    });

    it("/api/v1/cookies 没有登录不能获取cookie列表", () => {
        return request(server).get('/api/v1/cookies')
            .expect(200)
            .then(res => {
                expect(res.body.code).to.equal(500);
            });
    });

    it("/api/v1/cookies/:id 获取一个不存在的cookie", async function ()  {
        return request(server).get('/api/v1/cookies/' + 999999)
            .expect(200)
            .then(res => {
                expect(res.body.code).to.equal(500);
            });
    });

    it("/api/v1/cookies 创建重复cookie", async function ()  {
        let cookie =  generateCookie();
        let cookieIds = await db('cookies').insert(cookie);

        return request(server).post('/api/v1/cookies')
            .set('token', token)
            .send(cookie)
            .expect(200)
            .then(res => {
                expect(res.body.code).to.equal(500);
            });
    });

    it("/api/v1/cookies 更新cookie时没有提供id", async function ()  {
        let cookieNew = {username: 'new', password: 'new', cookie: 'new'};

        return request(server).put('/api/v1/cookies/')
            .set('token', token)
            .send(cookieNew)
            .expect(200)
            .then(res => {
                expect(res.body.code).to.equal(500);
            });
    });

});

