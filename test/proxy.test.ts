import request = require('supertest');
import server from "../src/server";
import 'mocha';
import { expect } from 'chai';

import knex = require('knex');
import config from "../src/config/index";

const db = knex({
    client: 'mysql',
    connection: config.mysql
});

describe("api test", () => {
    before(async function () {
        await db('cookies').del();
    });

    after(async function () {
    });

    it("get / 首页", () => {
        return request(server).get('/')
            .expect(302)
            .then(res => {
                console.log(res.body);

                // expect(res.body.code).to.equal(0);
            });
    });

    it("get /cn/ 首页", () => {
        return request(server).get('/cn/')
            .expect(200)
            .then(res => {
                console.log(res.body);

                // expect(res.body.code).to.equal(0);
            });
    });

    // Todo 浏览器可以登录，这里不能登录，未找到原因
    // it("登录", () => {
    //     return request(server).post('/cn/login')
    //         .send({userName: config.mc.username, password: config.mc.password})
    //         .expect(302)
    //         .then(async function (res) {
    //             console.log(res.header.location);
    //             console.log('body', res.body);
    //
    //             expect(res.header.location).to.equal('home.html');
    //             let cookies = await db('cookies').where({username: config.mc.username}).select();
    //             expect(cookies[0]).to.include({username: config.mc.username});
    //         });
    // });
});

