import app from "./app";
import config from "./config/index";

const server = app.listen(config.port, () => {
    console.log("  App is running at http://localhost:$s", config.port);
});

export default server;
