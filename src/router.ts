import Router from 'koa-router';
import jwt from 'jsonwebtoken';
import config from './config/index';

import proxyController from './controller/proxy/index';
import apiController from './controller/api/index';
import userService from './service/user';

const router = new Router();

/**
 * token验证
 */
async function verify(ctx, next) {
    let token = ctx.headers['token'];
    try {
        let decoded = jwt.verify(token, config.token_secret);
        ctx.state.userId = await userService.findById(decoded.userId);
        await next();
    } catch (err) {
        console.error(err);
        ctx.body = {
            code: 500,
            message: err.message
        };
    }
}

/**
 * 后台管理api
 */
router.post('/api/v1/signup', apiController.signup);
router.get('/api/v1/login', apiController.login);
router.get('/api/v1/cookies', verify, apiController.cookies);
router.get('/api/v1/cookies/:id', verify, apiController.cookiesOne);
router.post('/api/v1/cookies', verify, apiController.postCookies);
router.put('/api/v1/cookies', verify, apiController.putCookies);
router.delete('/api/v1/cookies/:id', verify, apiController.deleteCookies);

/**
 * 反向代理服务器
 */
router.get('/', proxyController.index);
router.post(/\/cn\/login/, proxyController.postLogin);
router.all(/\/cn/, proxyController.all);


export default router;
