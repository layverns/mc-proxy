import Koa from 'koa';
import path from 'path';
import logger from 'koa-logger';

import bodyParser from 'koa-bodyparser';
import serve from 'koa-static';

import router from './router';

const app = new Koa();

app.use(bodyParser());
app.use(logger());

app.use(router.routes()).use(router.allowedMethods());
app.use(serve(path.join(__dirname, './public')));
app.on('error', function (err, ctx) {
    console.log(err);
});

export default app;
