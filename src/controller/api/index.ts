import jwt from 'jsonwebtoken';

import userService from "../../service/user";
import cookieService from "../../service/cookie";
import config from "../../config/index";

class Controller {
    constructor () {

    }

    /**
     * @api {post} /api/v1/signup 注册
     * @apiGroup Cookie
     *
     * @apiParam {String} username 用户名.
     * @apiParam {String} password 密码.
     *
     * @apiSuccess {Number} code 错误代码.
     */
    async signup(ctx, next) {
        let username = ctx.request.body.username;
        let password = ctx.request.body.password;

        try {
            await userService.create(username, password);

            ctx.body = {
                code: 0
            };
        } catch (err) {
            console.error(err);

            ctx.body = {
                code: 500,
                message: err.message
            };
        }
    }

    /**
     * @api {get} /api/v1/login 登录
     * @apiGroup Cookie
     *
     * @apiParam {String} username 用户名.
     * @apiParam {String} password 密码.
     *
     * @apiSuccess {Number} code 错误代码.
     * @apiSuccess {Object} data 数据.
     * @apiSuccess {String} data.token token.
     */
    async login(ctx, next) {

        let username = ctx.query.username;
        let password = ctx.query.password;

        try {
            let userId = await userService.find(username, password);
            let token = jwt.sign({ userId }, config.token_secret);
            ctx.body = {
                code: 0,
                data: {
                    token
                }
            };
        } catch (err) {
            console.error(err);

            ctx.body = {
                code: 500,
                message: err.message
            };
        }
    }

    /**
     * @api {get} /api/v1/cookies 获取cookies列表
     * @apiGroup Cookie
     *
     * @apiHeader {String} token token
     *
     * @apiSuccess {Number} code 错误代码.
     * @apiSuccess {Object} data 数据.
     * @apiSuccess {Array} data.cookies cookies列表.
     */
    async cookies(ctx, next) {
        let limit = parseInt(ctx.query.limit || 10);
        let offset = parseInt(ctx.query.offset || 0);
        let orderBy = ctx.query.orderBy || 'id';
        let sort = ctx.query.sort || 'asc';

        try {
            let cookies = await cookieService.list(limit, offset, orderBy, sort);

            ctx.body = {
                code: 0,
                data: {
                    cookies
                }
            };
        } catch (err) {
            console.error(err);

            ctx.body = {
                code: 500,
                message: err.message
            };
        }
    }

    /**
     * @api {get} /api/v1/cookies/:id 获取一个cookie
     * @apiGroup Cookie
     *
     * @apiParam {String} id cookie id.
     *
     * @apiSuccess {Number} code 错误代码.
     * @apiSuccess {Object} data 数据.
     * @apiSuccess {Object} data.cookie cookie数据.
     */
    async cookiesOne(ctx, next) {
        let id = ctx.params.id;
        try {
            let cookie = await cookieService.findById(id);

            ctx.body = {
                code: 0,
                data: {
                    cookie
                }
            };
        } catch (err) {
            console.error(err);

            ctx.body = {
                code: 500,
                message: err.message
            };
        }
    }

    /**
     * @api {post} /api/v1/cookies 创建一个cookie
     * @apiGroup Cookie
     *
     * @apiParam {String} username 用户名.
     * @apiParam {String} password 密码.
     * @apiParam {String} cookie cookie.
     *
     * @apiSuccess {Number} code 错误代码.
     * @apiSuccess {Object} data 数据.
     * @apiSuccess {String} data.cookieId cookieId.
     */
    async postCookies(ctx, next) {
        let username = ctx.request.body.username;
        let password = ctx.request.body.password;
        let cookie = ctx.request.body.cookie;
        try {
            let cookies = await cookieService.create(username, password, cookie);

            ctx.body = {
                code: 0,
                data: {
                    cookieId: cookies[0]
                }
            };
        } catch (err) {
            console.error(err);

            ctx.body = {
                code: 500,
                message: err.message
            };
        }
    }


    /**
     * @api {put} /api/v1/cookies 更新一个cookie
     * @apiGroup Cookie
     *
     * @apiParam {String} username 用户名.
     * @apiParam {String} password 密码.
     * @apiParam {String} cookie cookie.
     * @apiParam {String} id cookie id.
     *
     * @apiSuccess {Number} code 错误代码.
     */
    async putCookies(ctx, next) {
        let username = ctx.request.body.username;
        let password = ctx.request.body.password;
        let cookie = ctx.request.body.cookie;
        let id = ctx.request.body.id;

        try {
            await cookieService.update(id, username, password, cookie);

            ctx.body = {
                code: 0,
                data: {
                }
            };
        } catch (err) {
            console.error(err);

            ctx.body = {
                code: 500,
                message: err.message
            };
        }
    }

    /**
     * @api {delete} /api/v1/cookies/:id 删除一个cookie
     * @apiGroup Cookie
     *
     * @apiParam {String} id cookie id.
     *
     * @apiSuccess {Number} code 错误代码.
     */
    async deleteCookies(ctx, next) {
        let id = ctx.params.id;

        try {
            await cookieService.deleteById(id);

            ctx.body = {
                code: 0,
                data: {
                }
            };
        } catch (err) {
            console.error(err);

            ctx.body = {
                code: 500,
                message: err.message
            };
        }
    }
}

export default new Controller();
