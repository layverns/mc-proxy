const httpProxy = require('http-proxy');
const _ = require('lodash');

import cookieService from "../../service/cookie";

class Controller {

    constructor () {

    }

    async index(ctx, next) {
        ctx.redirect('/cn/');
    }

    async postLogin(ctx, next) {
        let username: string = ctx.request.body.userName;
        let password: string = ctx.request.body.password;

        let cookie = await cookieService.find(username, password);
        if (!_.isEmpty(cookie)) {
            ctx.set('set-cookie', [cookie]);
            return ctx.redirect('/');
        }

        ctx.respond = false;
        const proxy = httpProxy.createProxyServer({
            secure: false,
        });

        proxy.on('error', function(e) {
            console.error(e);
        });
        proxy.on('proxyReq', function(proxyReq, req, res, options) {
            const body = ctx.request.rawBody;
            if (!_.isEmpty(body)) {
                proxyReq.write(body);
            }
        });
        proxy.on('proxyRes', function(proxyRes, req, res) {
            for (let cookie of proxyRes.headers['set-cookie']) {
                if (_.startsWith(cookie, 'JSESSIONID')) {
                    cookieService.save(username, password, cookie);
                }
            }
        });

        proxy.web(ctx.req, ctx.res, {
            target: 'https://www.4008-517-517.cn/',
            changeOrigin: true
        });
    }

    async all(ctx, next) {

        if (/\/cn\/signout/.test(ctx.req.url)) {
            let cookie = ctx.cookies.get('JSESSIONID');
            cookieService.del(cookie);
        }

        ctx.respond = false;
        const proxy = httpProxy.createProxyServer({
        });
        proxy.on('error', function(e) {
            console.error(e);
        });
        proxy.on('proxyReq', function(proxyReq, req, res, options) {
            const body = ctx.request.rawBody;
            if (!_.isEmpty(body)) {
                proxyReq.write(body);
            }
        });
        proxy.web(ctx.req, ctx.res, {
            target: 'https://www.4008-517-517.cn/',
            changeOrigin: true
        });
    }
}

export default new Controller();
