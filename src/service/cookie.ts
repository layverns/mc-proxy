
import knex from './knex';
import { encryptPassword } from './knex';

class Service {
    constructor () {

    }

    async save (username: string, password: string, cookie: string) {
        password = encryptPassword(password);
        try {
            let rows = await knex('cookies').select('username').where({username});

            if (rows.length > 0) {
                return await knex('cookies').where({username}).update({
                    password,
                    cookie
                });
            } else {
                let arr = await knex('cookies').insert({
                    username,
                    password,
                    cookie
                });
                return arr.length;
            }
        } catch (e) {
            console.error(e);
        }
    }

    async find (username: string, password: string) {
        password = encryptPassword(password);
        try {
            let rows = await knex('cookies').select('cookie').where({username, password});

            if (rows && rows.length > 0) {
                return rows[0].cookie;
            } else {
                return undefined;
            }
        } catch (e) {
            console.error(e);
            return undefined;
        }
    }

    async del (cookie: string) {
        try {
            return await knex('cookies').where('cookie', 'like', `%${cookie}%`).update({cookie: ''});
        } catch (e) {
            console.error(e);
            return 0;
        }
    }

    async create (username: string, password: string, cookie: string) {
        return await knex('cookies').insert({
            username,
            password,
            cookie
        });
    }

    async update (id: string, username: string, password: string, cookie: string) {
        return await knex('cookies').where({id}).update({
            username,
            password,
            cookie
        });
    }

    async list (limit: number, offset: number, orderBy: string, sort: string) {
        return await knex('cookies').limit(limit).offset(offset).orderBy(orderBy, sort).select();
    }

    async findById (id: string) {
        let cookies = await knex('cookies').where({id}).select();
        if (cookies.length === 0) {
            throw new Error('未查询到数据！');
        }
        return cookies[0];
    }

    async deleteById (cookieId: string) {
        return await knex('cookies').where({id: cookieId}).del();
    }

}

export default new Service();
