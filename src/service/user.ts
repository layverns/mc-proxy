import knex from './knex';
import { encryptPassword } from './knex';

class Service {
    constructor () {

    }

    async create (username: string, password: string) {
        password = encryptPassword(password);

        await knex('user').insert({username, password});
    }

    async find (username: string, password: string) {
        password = encryptPassword(password);

        let users = await knex('user').where({username}).select();

        if (users.length === 0) {
            throw new Error('该用户未注册！');
        }
        if (password !== users[0].password) {
            throw new Error('密码错误！');
        }

        return users[0].id;
    }

    async findById (userId: string) {

        let users = await knex('user').where({id: userId}).select();

        if (users.length === 0) {
            throw new Error('该用户未注册！');
        }

        return users[0].id;
    }

}

export default new Service();
