import knex = require('knex');
import crypto from 'crypto';

import config from "../config/index";

export default knex({
    client: 'mysql',
    connection: config.mysql
});

export function encryptPassword (password: string): string {
    if (!password) return '';
    try {
        return crypto.createHmac('sha256', 'sboefijsf').update(password).digest('hex');
    } catch (err) {
        console.error(err);
        return '';
    }
}
