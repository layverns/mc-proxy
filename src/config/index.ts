import configDefault from './config.default';
import configDev from './config.dev';
import configProd from './config.prod';
import _ from 'lodash';

let config = _.assignIn(configDefault, configDev);
if (process.env.NODE_ENV === 'production') {
    config = _.assignIn(configDefault, configProd);
}

console.log('env: ', process.env.NODE_ENV, config);
export default config;
